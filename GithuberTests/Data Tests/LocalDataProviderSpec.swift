//
//  LocalDataProviderSpec.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/8/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import Quick
import Nimble
@testable import Githuber

class LocalDataProviderSpec: QuickSpec {
    override func spec() {
        super.spec()
        var localDataProvider: LocalDataProviderType!
        beforeEach {
            localDataProvider = LocalDataProvider()
        }
        
        context("[UT]: Testing LocalDataProvider data saving functionality"){
            it("should save data successfully providing valid input") {
                let repoOwner = RepoOwner()
                repoOwner.name = "testuser"

                expect(repoOwner).notTo(beNil())
                let repo = Repo()
                repo.name = "test repo"
                repo.info = "A repo for testing purposes"
                repo.owner = repoOwner
                expect(repo).notTo(beNil())
                
                localDataProvider.setDefaultUser(username: repoOwner.name!)
                expect {
                    try localDataProvider.saveRepos([repo], for: repoOwner.name!)
                    }.notTo(throwAssertion())
                waitUntil { done in
                    localDataProvider
                        .fetchRepos(for: repoOwner.name!,
                                    onCompletion: { (repos, error) in
                        expect(repos).notTo(beNil())
                        expect(repos?.count).to(equal(1))
                        expect(repos?.first?.name).to(equal(repo.name))
                        expect(repos?.first?.info).to(equal(repo.info))
                        done()
                    })
                }
            }
        }
        
        afterEach {
            try? localDataProvider.purge()
            localDataProvider = nil
        }
    }
}
