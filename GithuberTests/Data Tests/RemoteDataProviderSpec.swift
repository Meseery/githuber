//
//  DataProviderSpec.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/8/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import Quick
import Nimble
@testable import Githuber

class RemoteDataProviderSpec: QuickSpec {
    override func spec() {
        var remoteDataProvider: RemoteDataProviderType!
        beforeEach {
            remoteDataProvider = RemoteDataProvider()
        }

        context("[UT]: Testing RemoteDataProvider data fetching functionality"){
            it("should fetch data successfully providing valid input") {
                waitUntil(timeout: 10.0) { done in
                    remoteDataProvider.fetchRepos(for: AppConstants.defaultUsername) { (result, error) in
                        expect(result).to(beAKindOf([Repo].self))
                        expect(result).notTo(beNil())
                        expect(error).to(beNil())
                        done()
                    }
                }
            }
        }

        afterEach {
            remoteDataProvider = nil
        }
    }
}
