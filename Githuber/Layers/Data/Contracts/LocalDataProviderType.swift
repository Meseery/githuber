//
//  LocalDataProviderType.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/6/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import Foundation

protocol LocalDataProviderType: DataProviderType {
    var isEmpty: Bool { get }
    func isUserChanged(newuser: String) -> Bool
    func setDefaultUser(username: String)
    func saveRepos(_ repos: [Repo], for user: String) throws
    func removeRepos(for user: String) throws
    func purge() throws
}
