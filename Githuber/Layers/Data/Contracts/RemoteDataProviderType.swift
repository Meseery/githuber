//
//  RemoteDataProviderType.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/6/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import Foundation

protocol RemoteDataProviderType: DataProviderType {
    var isAvailable: Bool { get }
}
