//
//  DataProviderType.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/6/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import Foundation

protocol DataProviderType {
    func fetchRepos(for user: String,
                    onCompletion:@escaping CompletionClosure<[Repo]>)
    func fetchCommits(for user: String, repo: String,
                    onCompletion:@escaping CompletionClosure<[Commit]>)
}
