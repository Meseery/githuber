//
//  ReposEndpoint.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/6/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import Alamofire

enum UserEndpoint: APIEndpointType {
    case userRepos(user: String)
    case userRepoCommits(user: String, repo: String)

    var baseURL: URL? {
        return URL(string: "https://api.github.com/") ?? URL(string: "")
    }

    var path: String {
        switch self {
        case let .userRepos(username):
            return "users/\(username)/repos"
        case let .userRepoCommits(username,reponame):
            return "repos/\(username)/\(reponame)/commits"
        }
    }

    var method: HTTPMethod {
        return .get
    }

    var parameters: [String : Any]? { return nil }
    var headers: [String : String]? { return nil }
}
