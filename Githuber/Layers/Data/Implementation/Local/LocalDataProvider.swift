//
//  LocalDataProvider.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/6/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import RealmSwift

class LocalDataProvider: LocalDataProviderType {
    var isEmpty: Bool {
        return dbProvider?.isEmpty ?? true
    }

    private var dbProvider: Realm? {
        guard let realmFileURL = fileURL(for: username) else { return nil }
        guard let dbProvider = try? Realm(fileURL: realmFileURL) else {
            return nil
        }
        return dbProvider
    }

    private var username: String? {
        get {
            return UserDefaults.standard.string(forKey: AppConstants.usernameKey)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: AppConstants.usernameKey)
        }
    }

    func saveRepos(_ repos: [Repo], for user: String) throws {
        try dbProvider?.write {
            dbProvider?.add(repos, update: true)
        }
    }

    func fetchRepos(for user: String,
                    onCompletion: @escaping CompletionClosure<[Repo]>) {
        let predicate = NSPredicate(format: "%K CONTAINS[c] %@", "owner.name", user)
        let repos = dbProvider?.objects(Repo.self).filter(predicate)
        onCompletion(repos?.compactMap({$0}), nil)
    }
    
    func removeRepos(for user: String) throws {
        let predicate = NSPredicate(format: "%K == %@", "owner.name", user)
        if let repos = dbProvider?.objects(Repo.self).filter(predicate) {
            try dbProvider?.write {
                dbProvider?.delete(repos)
            }
        }
    }
    
    func fetchCommits(for user: String, repo: String, onCompletion: @escaping CompletionClosure<[Commit]>) {}
    
    func purge() throws {
        let realmURL = dbProvider?.configuration.fileURL!
        let realmURLs = [
            realmURL,
            realmURL?.appendingPathExtension("lock"),
            realmURL?.appendingPathExtension("note"),
            realmURL?.appendingPathExtension("management")
        ]
        for URL in realmURLs.compactMap({$0}) {
            do {
                try FileManager.default.removeItem(at: URL)
            } catch let error {
                throw error
            }
        }
    }
    
    func setDefaultUser(username: String) {
        self.username = username
    }

    func isUserChanged(newuser: String) -> Bool {
        return username != newuser
    }

    private func fileURL(for user: String?) -> URL? {
        guard let user = user else { return nil }
        guard let fileURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first?.appendingPathComponent("\(user).realm") else { return nil }
        return fileURL
    }
}
