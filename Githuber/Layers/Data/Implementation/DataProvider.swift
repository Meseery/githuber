//
//  DataProvider.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/6/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import Alamofire
import RealmSwift

class DataProvider: DataProviderType {
    private var localDataProvider: LocalDataProviderType
    private var remoteDataProvider: RemoteDataProviderType
    
    init(localDataProvider: LocalDataProviderType,
         remoteDataProvider: RemoteDataProviderType) {
        self.remoteDataProvider = remoteDataProvider
        self.localDataProvider = localDataProvider
    }
    
    func fetchRepos(for user: String,
                    onCompletion: @escaping CompletionClosure<[Repo]>) {
        if localDataProvider.isEmpty || localDataProvider.isUserChanged(newuser: user) {
            guard remoteDataProvider.isAvailable else {
                onCompletion(nil, .noInternetConection)
                return
            }

            remoteDataProvider.fetchRepos(for: user) { [weak self] (repos, error) in
                guard let `self` = self else { return }
                guard let repos = repos else {
                    onCompletion(nil, error)
                    return
                }
                try? self.localDataProvider.purge()
                self.localDataProvider.setDefaultUser(username: user)
                try? self.localDataProvider.saveRepos(repos, for: user)
                onCompletion(repos, nil)
            }
        } else {
            localDataProvider.fetchRepos(for: user, onCompletion: onCompletion)
        }
    }

    func fetchCommits(for user: String,
                      repo: String,
                      onCompletion: @escaping CompletionClosure<[Commit]>) {
        remoteDataProvider.fetchCommits(for: user,
                                        repo: repo,
                                        onCompletion: onCompletion)
    }
}
