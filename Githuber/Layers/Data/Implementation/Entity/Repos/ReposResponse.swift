//
//  ReposResponse.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/6/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import Foundation

class ReposResponse: Decodable {
    var repos: [Repo]?
    required init(from decoder: Decoder) throws {
        let map = try decoder.singleValueContainer()
        repos = try map.decode([Repo].self)
    }
}
