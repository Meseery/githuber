//
//  RepoOwner.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/7/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import RealmSwift

@objcMembers
class RepoOwner: Object, Decodable {
    dynamic var name: String?
    dynamic var avatarUrl: String?
    
    override class func primaryKey() -> String? {
        return "name"
    }
    
    private enum CodingKeys: String, CodingKey {
        case avatarUrl = "avatar_url"
        case name = "login"
    }
}
