//
//  Repo.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/6/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import RealmSwift

@objcMembers
class Repo: Object, Decodable {
    dynamic var repoId: Int = 0
    dynamic var name: String?
    dynamic var info: String?
    dynamic var forksCount: Int = 0
    dynamic var starsCount: Int = 0
    dynamic var commitsUrl: String?
    dynamic var owner: RepoOwner?

    override class func primaryKey() -> String? {
        return "repoId"
    }

    private enum CodingKeys: String, CodingKey {
        case repoId = "id"
        case name
        case info = "description"
        case forksCount = "forks_count"
        case starsCount = "stargazers_count"
        case commitsUrl = "commits_url"
        case owner = "owner"
    }
}


