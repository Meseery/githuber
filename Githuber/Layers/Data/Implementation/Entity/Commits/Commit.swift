//
//  Commit.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/7/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import Foundation

class Commit: Decodable {
    let sha: String?
    let message: String?
    let committer: Committer?
    enum CodingKeys: String, CodingKey {
        case sha
        case commit
    }
    
    enum CommitCodingKeys: String, CodingKey {
        case message
        case committer
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        sha = try container.decode(String.self, forKey: .sha)
        let commitContainer = try container.nestedContainer(keyedBy: CommitCodingKeys.self, forKey: .commit)
        message = try commitContainer.decode(String.self, forKey: .message)
        committer = try commitContainer.decode(Committer.self, forKey: .committer)
    }
}
