//
//  Committer.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/7/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import Foundation

class Committer: Decodable {
    let name: String?
    let email: String?
    let date: String?
}
