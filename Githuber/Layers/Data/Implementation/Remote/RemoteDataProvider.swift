//
//  RemoteDataProvider.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/6/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import Alamofire

class RemoteDataProvider: RemoteDataProviderType {
    var isAvailable: Bool {
        return NetworkReachabilityManager()?.isReachable ?? false
    }

    func fetchRepos(for user: String,
                    onCompletion:@escaping CompletionClosure<[Repo]>){
        let reposEndpoint = UserEndpoint.userRepos(user: user)
        reposEndpoint.execute(responseType: ReposResponse.self) { (response, error) in
            if error != nil {
                onCompletion(nil, error)
                return
            }
            
            if response != nil {
                onCompletion(response?.repos, nil)
                return
            }
        }
    }

    func fetchCommits(for user: String,
                      repo: String,
                      onCompletion: @escaping CompletionClosure<[Commit]>) {
        let commitsEndpoint = UserEndpoint.userRepoCommits(user: user,
                                                            repo: repo)
        commitsEndpoint.execute(responseType: [Commit].self) { (commits, error) in
            if error != nil {
                onCompletion(nil, error)
                return
            }
            
            if commits != nil {
                onCompletion(commits, nil)
                return
            }
        }
    }
}
