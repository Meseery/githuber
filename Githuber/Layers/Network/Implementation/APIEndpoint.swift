//
//  APIEndpoint.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/6/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import Alamofire

typealias CompletionClosure<T> = (T?, AppError?) -> Void

extension APIEndpointType {
    func execute<U: Decodable>(responseType: U.Type,
                               manager:SessionManager? = SessionManager.default,
                               extraHeaders: [String:String]? = nil,
                               onCompletion: @escaping CompletionClosure<U>) {
        
        guard let path = self
            .baseURL?
            .appendingPathComponent(self.path) else {
                onCompletion(nil, .invalidEndpoint)
                return
        }

        var requestHeaders = self.headers
        if let extraHeaders = extraHeaders {
            requestHeaders?.merge(extraHeaders, uniquingKeysWith: {return $1})
        }
        
        let request = manager?.request(path,
                                       method: self.method,
                                       parameters: self.parameters,
                                       headers: requestHeaders)
            request?
            .validate()
            .responseData { response in
                if response.result.isSuccess,
                    let data = response.result.value {
                    do {
                        let results = try DecodableParser.parseData(data: data,
                                                                to: U.self)
                        onCompletion(results, nil)
                    } catch {
                        onCompletion(nil, .parsingError)
                    }
                } else if response.result.isFailure {
                    if let statusCode = response.response?.statusCode {
                        onCompletion(nil,.networkError(statusCode))
                    }
                    onCompletion(nil, .networkException)
                }
        }
    }
}
