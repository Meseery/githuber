//
//  File.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/6/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import Foundation

protocol DecodableParserType {
    static func parseData<U: Decodable>(data: Data, to type: U.Type) throws -> U?
}
