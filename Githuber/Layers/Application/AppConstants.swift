//
//  AppConstants.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/7/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import Foundation

enum AppConstants {
    static let usernameKey = "username"
    static let defaultUsername = "mralexgray"
    static let defaultErrorTitle = "OoO"
    static let defaultAlertTitle = "Hi!"
    static let defaultUsernameAlertMessage = "Please enter username to fetch repos"
    static let defaultErrorTime = 5
    static let defaultEmptyMessage = "No Data Found"
}
