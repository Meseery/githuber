//
//  ViewStateful.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/8/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import Foundation

protocol ViewStateful {
    var viewState: ViewState { get set }
}
