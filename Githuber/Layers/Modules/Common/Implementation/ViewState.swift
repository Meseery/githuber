//
//  ViewState.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/8/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import Foundation

enum ViewState {
    case showLoading
    case hideLoading
    case showEmpty
    case hideEmpty
    case showData
    case showError(error: AppError)
}
