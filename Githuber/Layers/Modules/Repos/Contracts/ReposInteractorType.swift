//
//  ReposInteractor.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/6/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import Foundation

protocol ReposInteractorType: AnyObject {
    var reposOutput: ReposInteractorOutputType? { get }
    var dataProvider: DataProviderType { get }
    func fetchRepos(for user:String)
    func repo(with name:String) -> Repo?
}

protocol ReposInteractorOutputType: AnyObject {
    func reposFetched(_ repos: [Repo])
    func reposFetchFailed(with error:AppError)
}
