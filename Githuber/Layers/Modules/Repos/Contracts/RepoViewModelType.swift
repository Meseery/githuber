//
//  RepoCell.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/6/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import UIKit

protocol RepoViewModelType {
    var name: String { get }
    var stars: String { get }
    var forks: String { get }
    var info: String { get }
}
