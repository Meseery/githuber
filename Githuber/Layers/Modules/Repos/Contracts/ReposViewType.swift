//
//  RepoView.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/6/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import Foundation

protocol ReposViewType: LoadingViewType, EmptyViewType, ErrorViewType {
    var presenter: ReposPresenterType? { get set }
    func showUsernameEntry(default username:String)
    func dismissUsernameEntry()
    func setViewTitle(title: String)
    func showRepos()
}
