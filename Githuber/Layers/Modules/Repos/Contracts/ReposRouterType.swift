//
//  ReposRouter.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/6/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import UIKit

protocol ReposRouterType: AnyObject {
    var viewController: UIViewController? { get }
    static func buildModule() -> UIViewController
    func presentCommits(for repo: Repo)
}
