//
//  RepoPresenter.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/6/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import Foundation

protocol ReposPresenterType: AnyObject {
    var view: ReposViewType? { get set }
    var interactor: ReposInteractorType? { get set }
    var router: ReposRouterType? { get set }
    var reposCount: Int { get }
    
    func didEnterUsername(username: String)
    func viewDidLoad()
    func repoViewModel(atIndex index: Int) -> RepoViewModelType?
    func didSelectRepo(atIndex index: Int)
}
