//
//  ReposRouter.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/6/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import UIKit

class ReposRouter: ReposRouterType {
    var viewController: UIViewController?
    
    static func buildModule() -> UIViewController {
        let view = ReposViewController.instantiate()
        let presenter = ReposPresenter()
        let localDataProvider = LocalDataProvider()
        let remoteDataProvider = RemoteDataProvider()
        let dataProvider = DataProvider(localDataProvider: localDataProvider,
                                        remoteDataProvider: remoteDataProvider)
        let interactor = ReposInteractor(dataProvider: dataProvider, output: presenter)
        let router = ReposRouter()
        
        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        router.viewController = view
        
        return view
    }
    
    func presentCommits(for repo: Repo) {
        let commitsView = CommitsRouter.buildModule(with: repo)
        viewController?.navigationController?.pushViewController(commitsView, animated: true)
    }
}
