//
//  RepoTableViewCell.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/6/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import UIKit

class RepoTableViewCell: UITableViewCell {

    // MARK: - Outlets and variables
    @IBOutlet weak var repoNameLabel: UILabel!
    @IBOutlet weak var forksLabel: UILabel!
    @IBOutlet weak var starsLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    
    func setup(with repo: RepoViewModelType?) {
        repoNameLabel.text = repo?.name
        forksLabel.text = repo?.forks
        starsLabel.text = repo?.stars
        infoLabel.text = repo?.info
    }
}

