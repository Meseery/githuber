//
//  ReposViewController.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/6/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import UIKit

class ReposViewController: UIViewController {
    
    // MARK: - Outlets and variables
    @IBOutlet weak var reposTableView: UITableView! {
        didSet {
            setupTableView()
        }
    }

    var presenter: ReposPresenterType?

    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }
    
    // MARK: - Private functions
    private func setupTableView() {
        reposTableView.dataSource = self
        reposTableView.delegate = self
        reposTableView.rowHeight = UITableView.automaticDimension
        reposTableView.estimatedRowHeight = 300
        let nib = UINib(nibName: "RepoTableViewCell", bundle: nil)
        reposTableView.register(nib,
                                forCellReuseIdentifier: RepoTableViewCell.identifier)
    }
}

// MARK: - Instantiation

extension ReposViewController: StoryboardInstantiatable {
    static var storyboardName: String {
        return "ReposModule"
    }
}

// MARK: - Repos View

extension ReposViewController: ReposViewType {
    func setViewTitle(title: String) {
        self.title = title
    }
    
    func showUsernameEntry(default username: String) {
        let alertView = UIAlertController(title: AppConstants.defaultAlertTitle,
                                          message: AppConstants.defaultUsernameAlertMessage,
                                          preferredStyle: .alert)
        alertView.addTextField { [weak self] (textField) in
            guard let `self` = self else { return }
            textField.text = username
            textField.delegate = self
            textField.returnKeyType = .done
        }
        present(alertView, animated: true, completion: nil)
    }

    func dismissUsernameEntry(){
        dismiss(animated: true, completion: nil)
    }
    
    func showEmptyView(message: String) {
        reposTableView.showEmptyView(message: message)
    }
    
    func hideEmptyView() {
        reposTableView.hideEmptyView()
    }
    
    func showRepos() {
        reposTableView.reloadData()
    }
}

extension ReposViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let text = textField.text, !text.isEmpty else {
            return false
        }
        presenter?.didEnterUsername(username: text)
        return true
    }
}

// MARK: - Table View Data Source

extension ReposViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return presenter?.reposCount ?? 0
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: RepoTableViewCell.identifier) as! RepoTableViewCell
        cell.setup(with: presenter?.repoViewModel(atIndex: indexPath.row))
        return cell
    }
}

// MARK: - Table View Delegate

extension ReposViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        presenter?.didSelectRepo(atIndex: indexPath.row)
    }
}
