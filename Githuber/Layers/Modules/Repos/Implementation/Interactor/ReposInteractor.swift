//
//  ReposInteractor.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/6/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import Foundation

class ReposInteractor: ReposInteractorType {
    var reposOutput: ReposInteractorOutputType?
    var dataProvider: DataProviderType
    
    private var repos: [Repo]?

    init(dataProvider: DataProviderType, output: ReposInteractorOutputType) {
        self.dataProvider = dataProvider
        self.reposOutput = output
    }

    func fetchRepos(for user: String) {
        dataProvider.fetchRepos(for: user) { [weak self] (repos, error) in
            guard let `self` = self else { return }
            if let error = error{
                self.reposOutput?.reposFetchFailed(with: error)
                return
            }
            if let repos = repos {
                self.repos = repos
                self.reposOutput?.reposFetched(repos)
            }
        }
    }

    public func repo(with name:String) -> Repo? {
        return repos?.first {$0.name == name}
    }
}
