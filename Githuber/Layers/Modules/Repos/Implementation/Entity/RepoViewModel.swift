//
//  RepoViewModel.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/6/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import Foundation

struct RepoViewModel: RepoViewModelType {
    var name: String
    var forks: String
    var stars: String
    var info: String
    
    init(repo: Repo) {
        self.name = repo.name ?? ""
        self.forks = String(repo.forksCount)
        self.stars = String(repo.starsCount)
        self.info = repo.info ?? ""
    }
}
