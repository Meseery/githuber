//
//  ReposPresenter.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/6/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import Foundation

class ReposPresenter {
    var view: ReposViewType?
    var interactor: ReposInteractorType?
    var router: ReposRouterType?
    
    private var viewState: ViewState = .showLoading {
        didSet {
            switch viewState {
            case .showEmpty:
                view?.showEmptyView(message: AppConstants.defaultEmptyMessage)
            case .hideEmpty:
                view?.hideEmptyView()
            case .showLoading:
                view?.showLoading()
            case .hideLoading:
                view?.hideLoading()
            case let .showError(error):
                view?.showError(error, for: 4)
            case .showData:
                view?.showRepos()
            }
        }
    }
    
    private var reposViewModels: [RepoViewModelType]? {
        didSet {
            guard let reposViewModels = reposViewModels else {
                return
            }
            viewState = reposViewModels.count > 0 ? .showData : .showEmpty
        }
    }
}

extension ReposPresenter: ReposPresenterType {
    var reposCount: Int {
        return reposViewModels?.count ?? 0
    }
    
    func didEnterUsername(username: String) {
        view?.dismissUsernameEntry()
        view?.setViewTitle(title: "\(username) Repos")
        viewState = .hideEmpty
        viewState = .showLoading
        interactor?.fetchRepos(for: username)
    }

    func viewDidLoad() {
        viewState = .showEmpty
        view?.showUsernameEntry(default: AppConstants.defaultUsername)
    }

    func repoViewModel(atIndex index: Int) -> RepoViewModelType? {
        return reposViewModels?[index]
    }

    func didSelectRepo(atIndex index: Int) {
        guard let repoViewModel = reposViewModels?[index] else { return }
        guard let repo = interactor?.repo(with: repoViewModel.name) else { return }
        router?.presentCommits(for: repo)
    }
}

extension ReposPresenter: ReposInteractorOutputType {
    func reposFetched(_ repos: [Repo]) {
        let reposViewModels = repos.compactMap {RepoViewModel.init(repo: $0)}
        viewState = .hideLoading
        self.reposViewModels = reposViewModels
    }
    
    func reposFetchFailed(with error: AppError) {
        viewState = .hideLoading
        viewState = .showError(error: error)
        viewState = .showEmpty
    }
}
