//
//  CommitTableViewCell.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/8/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import UIKit

class CommitTableViewCell: UITableViewCell {
    // MARK: - Outlets and variables
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var committerLabel: UILabel!
    
    func setup(with commit: CommitViewModelType?) {
        committerLabel.text = commit?.committer
        messageLabel.text = commit?.message
    }
}
