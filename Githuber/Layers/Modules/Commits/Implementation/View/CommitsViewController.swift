//
//  CommitsViewController.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/8/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import UIKit

class CommitsViewController: UIViewController {
    // MARK: - Outlets and variables
    @IBOutlet weak var commitsTableView: UITableView! {
        didSet {
            setupTableView()
        }
    }
    var presenter: CommitsPresenterType?

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }
    
    // MARK: - Private functions
    private func setupTableView() {
        commitsTableView.dataSource = self
        commitsTableView.rowHeight = UITableView.automaticDimension
        commitsTableView.estimatedRowHeight = 300
        let nib = UINib(nibName: "CommitTableViewCell", bundle: nil)
        commitsTableView.register(nib,
                                forCellReuseIdentifier: CommitTableViewCell.identifier)
    }
}

// MARK: - Instantiation

extension CommitsViewController: StoryboardInstantiatable {
    static var storyboardName: String {
        return "ReposModule"
    }
}


extension CommitsViewController: CommitsViewType {
    func setViewTitle(title: String) {
        self.title = title
    }

    func showCommits() {
        commitsTableView.reloadData()
    }

    func showEmptyView(message: String) {
        commitsTableView.showEmptyView(message: message)
    }
    
    func hideEmptyView() {
        commitsTableView.hideEmptyView()
    }
}

extension CommitsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return presenter?.commitsCount ?? 0
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CommitTableViewCell.identifier) as! CommitTableViewCell
        cell.setup(with: presenter?.commitViewModel(atIndex: indexPath.row))
        return cell
    }
}
