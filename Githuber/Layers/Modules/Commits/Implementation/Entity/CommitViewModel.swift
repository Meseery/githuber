//
//  CommitViewModel.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/8/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import Foundation

struct CommitViewModel: CommitViewModelType {
    var message: String
    var committer: String
    
    init(commit: Commit) {
        self.message = commit.message ?? ""
        self.committer = commit.committer?.name ?? ""
    }
}
