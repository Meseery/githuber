//
//  CommitsInteractor.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/8/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import Foundation

class CommitsInteractor: CommitsInteractorType {
    var commitsOutput: CommitsInteractorOutputType?
    var dataProvider: DataProviderType
    
    private var commits: [Commit]?
    
    init(dataProvider: DataProviderType, output: CommitsInteractorOutputType) {
        self.dataProvider = dataProvider
        self.commitsOutput = output
    }
    
    func fetchCommits(for user: String, repo: String) {
        dataProvider.fetchCommits(for: user,
                                  repo: repo) { [weak self] (commits, error) in
                                    guard let `self` = self else { return }
                                    if let error = error{
                                        self.commitsOutput?.commitsFetchFailed(with: error)
                                        return
                                    }
                                    if let commits = commits {
                                        self.commits = commits
                                        self.commitsOutput?.commitsFetched(commits)
                                    }
        }
    }
}
