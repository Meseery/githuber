//
//  CommitsRouter.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/8/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import UIKit

class CommitsRouter: CommitsRouterType {    
    static func buildModule(with repo: Repo) -> UIViewController {
        let view = CommitsViewController.instantiate()
        let presenter = CommitsPresenter(repo: repo)
        let localDataProvider = LocalDataProvider()
        let remoteDataProvider = RemoteDataProvider()
        let dataProvider = DataProvider(localDataProvider: localDataProvider,
                                        remoteDataProvider: remoteDataProvider)
        let interactor = CommitsInteractor(dataProvider: dataProvider, output: presenter)
        let router = CommitsRouter()
        
        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        return view
    }
}
