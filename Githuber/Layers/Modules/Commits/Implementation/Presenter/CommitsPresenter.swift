//
//  CommitsPresenter.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/8/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import Foundation

class CommitsPresenter {
    var repo: Repo
    var view: CommitsViewType?
    var interactor: CommitsInteractorType?
    var router: CommitsRouterType?
    
    var commitsCount: Int {
        return commitsViewModels?.count ?? 0
    }
    
    init(repo: Repo) {
        self.repo = repo
    }

    private var viewState: ViewState = .showLoading {
        didSet {
            switch viewState {
            case .showEmpty:
                view?.showEmptyView(message: AppConstants.defaultEmptyMessage)
            case .hideEmpty:
                view?.hideEmptyView()
            case .showLoading:
                view?.showLoading()
            case .hideLoading:
                view?.hideLoading()
            case let .showError(error):
                view?.showError(error, for: 4)
            case .showData:
                view?.showCommits()
            }
        }
    }
    
    private var commitsViewModels: [CommitViewModelType]? {
        didSet {
            guard let commitsViewModels = commitsViewModels else {
                return
            }
            viewState = commitsViewModels.count > 0 ? .showData : .showEmpty
        }
    }
}

extension CommitsPresenter: CommitsPresenterType {
    func viewDidLoad() {
        view?.setViewTitle(title: "\(repo.name ?? "Repo") Commits")
        viewState = .showLoading
        guard let reponame = repo.name, let ownername = repo.owner?.name else {
            viewState = .showError(error: .invalidData)
            return
        }
        interactor?.fetchCommits(for: ownername, repo: reponame)
    }
    
    func commitViewModel(atIndex index: Int) -> CommitViewModelType? {
        return commitsViewModels?[index]
    }
}

extension CommitsPresenter: CommitsInteractorOutputType {
    func commitsFetched(_ commits: [Commit]) {
        viewState = .hideLoading
        let commitsViewModels = commits.compactMap {CommitViewModel.init(commit: $0)}
        self.commitsViewModels = commitsViewModels
    }

    func commitsFetchFailed(with error: AppError) {
        viewState = .hideLoading
        viewState = .showError(error: error)
        viewState = .showEmpty
    }
}
