//
//  CommitsRouterType.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/8/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import UIKit

protocol CommitsRouterType {
    static func buildModule(with repo: Repo) -> UIViewController
}
