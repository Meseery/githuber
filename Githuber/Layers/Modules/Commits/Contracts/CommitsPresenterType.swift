//
//  CommitsPresenterType.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/8/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import Foundation

protocol CommitsPresenterType {
    var view: CommitsViewType? { get set }
    var interactor: CommitsInteractorType? { get set }
    var router: CommitsRouterType? { get set }
    
    var repo: Repo { get set }
    var commitsCount: Int { get }
    
    func viewDidLoad()
    func commitViewModel(atIndex index: Int) -> CommitViewModelType?
}
