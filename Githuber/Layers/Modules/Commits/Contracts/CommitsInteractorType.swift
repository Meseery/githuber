//
//  CommitsInteractorType.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/8/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import Foundation

protocol CommitsInteractorType {
    var commitsOutput: CommitsInteractorOutputType? { get }
    var dataProvider: DataProviderType { get }
    func fetchCommits(for user: String, repo: String)
}

protocol CommitsInteractorOutputType: AnyObject {
    func commitsFetched(_ commits: [Commit])
    func commitsFetchFailed(with error:AppError)
}
