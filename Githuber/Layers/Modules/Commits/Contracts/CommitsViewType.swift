//
//  CommitViewType.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/8/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import Foundation

protocol CommitsViewType: LoadingViewType, EmptyViewType, ErrorViewType {
    var presenter: CommitsPresenterType? { get set }
    func setViewTitle(title: String)
    func showCommits()
}
