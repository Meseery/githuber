//
//  CommitViewModelType.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/8/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import Foundation

protocol CommitViewModelType {
    var message: String { get }
    var committer: String { get }
}
