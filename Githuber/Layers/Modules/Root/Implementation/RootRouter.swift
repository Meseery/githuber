//
//  RootRouter.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/6/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import UIKit

class RootRouter: RootRouterType {
    static func presentRootScreen(in window: UIWindow) {
        let view = ReposRouter.buildModule()
        let navigation = UINavigationController(rootViewController: view)
        window.rootViewController = navigation
        window.makeKeyAndVisible()
    }
}
