//
//  RootRouter.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/6/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import UIKit

protocol RootRouterType: AnyObject {
    static func presentRootScreen(in window: UIWindow)
}
