//
//  EmptyView.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/6/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import Foundation

protocol EmptyViewType {
    func showEmptyView(message: String)
    func hideEmptyView()
}
