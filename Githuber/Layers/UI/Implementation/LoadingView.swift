//
//  LoadingView.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/6/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import UIKit

class LoadingView: UIView {
    private var activityIndicatorView: UIActivityIndicatorView!
    private var titleLabel: UILabel!
    
    init(frame: CGRect,
         color: UIColor? = .black,
         message: String? = nil) {
        super.init(frame: frame)
        self.backgroundColor = .clear
        self.alpha = 0.0
        
        activityIndicatorView = UIActivityIndicatorView(style: .whiteLarge)
        activityIndicatorView?.color = color
        
        titleLabel = UILabel(frame: CGRect(x: 0,
                                           y: 0,
                                           width: UIScreen.main.bounds.width,
                                           height: 100.0))
        titleLabel?.text = message
        titleLabel?.textColor = .black
        
        addSubview(activityIndicatorView)
        addSubview(titleLabel)
        setupLayout()
    }
    
    func setupLayout() {
        activityIndicatorView.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        activityIndicatorView.centerXAnchor.constraint(equalTo: centerXAnchor)
            .isActive = true
        activityIndicatorView.centerYAnchor.constraint(equalTo: centerYAnchor)
            .isActive = true
        titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor)
            .isActive = true
        titleLabel.centerYAnchor.constraint(equalTo: activityIndicatorView.centerYAnchor, constant: 55.0)
            .isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func present() {
        guard let window = UIApplication.shared.keyWindow else { return }
        window.addSubview(self)
    }
    
    func dismiss() {
        fadeOut()
    }
    
    static func show(with message: String = "Loading ... ") {
        guard let window = UIApplication.shared.keyWindow else { return }
        let loadingView = LoadingView(frame: window.frame,
                                      color: .darkGray,
                                      message: message)
        window.addSubview(loadingView)
        loadingView.fadeIn()
    }
    
    static func dismiss() {
        guard let window = UIApplication.shared.keyWindow else { return }
        guard let loadingView = window.subviews.first(where: {$0 is LoadingView}) as? LoadingView else { return }
        loadingView.dismiss()
    }
    
    private func fadeIn() {
        UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseInOut, animations: {
            self.alpha = 1.0
            self.activityIndicatorView.startAnimating()
        })
    }
    
    private func fadeOut() {
        UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseInOut, animations: {
            self.activityIndicatorView.stopAnimating()
            self.alpha = 0.0
        }, completion: { (_: Bool) in
            self.removeFromSuperview()
        })
    }
}
