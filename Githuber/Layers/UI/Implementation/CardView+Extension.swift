//
//  CardView+Extension.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/6/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import UIKit

extension CardViewType {
    func applyCardStyle() {
        self.layer.cornerRadius = 15.0
        self.layer.borderColor  =  UIColor.clear.cgColor
        self.layer.borderWidth = 5.0
        self.layer.shadowOpacity = 0.5
        self.layer.shadowColor =  UIColor.lightGray.cgColor
        self.layer.shadowRadius = 5.0
        self.layer.shadowOffset = CGSize(width: 15,
                                         height: 15)
        self.layer.masksToBounds = true
    }
}

extension UIView: CardViewType {}
