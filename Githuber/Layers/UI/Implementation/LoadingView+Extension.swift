//
//  LoadingView+Extension.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/6/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import UIKit

extension LoadingViewType where Self: UIViewController{
    func showLoading() {
        LoadingView.show()
    }

    func hideLoading() {
        LoadingView.dismiss()
    }
}
