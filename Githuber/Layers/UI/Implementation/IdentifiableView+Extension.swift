//
//  IdentifiableView+Extension.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/6/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import UIKit

extension IdentifiableViewType {
    static var identifier: String {
        return String(describing: self)
    }
}

extension UIViewController: IdentifiableViewType {}
extension UITableViewCell: IdentifiableViewType {}
