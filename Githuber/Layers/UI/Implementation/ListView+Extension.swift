//
//  EmptyView+Extension.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/6/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import UIKit

extension UITableView: ListViewType {}
extension UICollectionView: ListViewType {}

extension ListViewType {
    func showEmptyView(message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0,
                                                 y: 0,
                                                 width: bounds.size.width,
                                                 height: bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = .black
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont.boldSystemFont(ofSize: 16.0)
        messageLabel.sizeToFit()
        backgroundView = messageLabel
        if let `self` = self as? UITableView {
            self.separatorStyle = .none
        }
    }
    
    func hideEmptyView() {
        backgroundView = nil
        if let `self` = self as? UITableView {
            self.separatorStyle = .singleLine
        }
    }
}
