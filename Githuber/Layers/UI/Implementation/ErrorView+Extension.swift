//
//  ErrorView+Extension.swift
//  Githuber
//
//  Created by Mohamed EL Meseery on 4/6/19.
//  Copyright © 2019 Mohamed EL Meseery. All rights reserved.
//

import UIKit

extension ErrorViewType where Self: UIViewController {
    func showError(_ error: Error,
                   for seconds: TimeInterval?) {
        showAlert(title: AppConstants.defaultErrorTitle,
                  message: error.localizedDescription,
                  dismissAfter: Int(seconds ?? 5))
    }
}

extension UIViewController {
    public func showAlert(title: String,
                          message: String,
                          dismissAfter seconds: Int? = nil) {
        let alertViewController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        present(alertViewController, animated: true)
        if let seconds = seconds {
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(seconds)) {
                alertViewController.dismiss(animated: true)
            }
        }
    }
}
